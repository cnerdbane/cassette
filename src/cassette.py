import asyncio
import json
import os
import re
import redis
import discord
from discord.ext import commands
from discord.ext.commands.core import command
from random import randrange
from urlhandlers import YoutubeHandler, SoundcloudHandler

MSG_MUSIC_STOP = "Music Stopped and Queue Cleared"
MSG_VOICE_NOT_CONNECTED = "You're not connected to a voice channel..."
MSG_PROCESSING_PLAYLIST = "Processing playlist..."
MSG_PROCESSING_PLAYLIST_DONE = f"{MSG_PROCESSING_PLAYLIST}Done!"
MAX_MESSAGE_LENGTH = 2000 # max discord message length in characters

class Cassette(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.is_playing = False
        self.shuffle = False
        self.vc = ""
        self.last_text_channel = ""
        self.FFMPEG_OPTIONS = {"before_options": "-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5", "options": "-vn"}

        self.handlers = [
            ["youtube.com/watch.+&list=", (YoutubeHandler, self.HandlePlaylist)],
            ["youtube.com/watch", (YoutubeHandler, self.HandleSong)],
            ["youtu.be/", (YoutubeHandler, self.HandleSong)],
            ["youtube.com/playlist", (YoutubeHandler, self.HandlePlaylist)],
            ["soundcloud.com/.+/sets/", (SoundcloudHandler, self.HandlePlaylist)], # needs to go first until i get negative matching
            ["soundcloud.com/", (SoundcloudHandler, self.HandleSong)],
        ]

    async def HandleSong(self, url_handler, query, ctx, voice_channel): 
        title = url_handler.get_song_title(query)
        self.append_queue([query, title, voice_channel.id])
        await self.send_message(ctx, f"{title} Added to the Queue")
        if self.is_playing == False:
            await self.play_music(ctx)

    async def HandlePlaylist(self, url_handler, query, ctx, voice_channel):
        message = await self.send_message(ctx, MSG_PROCESSING_PLAYLIST)
        playlist = url_handler.get_playlist_urls(query)

        for song in playlist:
            url = song[0]
            title = song[1]
            self.append_queue([url, title, voice_channel.id])

        await message.edit(content = MSG_PROCESSING_PLAYLIST_DONE)

        if self.is_playing == False:
            await self.play_music(ctx)

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        if not member.id == self.bot.user.id:
            return
        elif before.channel is None:
            time = 0
            while True:
                await asyncio.sleep(1)
                time = time + 1
                if self.is_playing:
                    time = 0
                if time == 300:
                    await self.vc.disconnect()
                if not self.vc.is_connected():
                    break

    def play_next(self):
        music_queue = self.get_queue()

        if len(music_queue) > 0:
            self.is_playing = True

            next_song_index = 0
            if self.shuffle:
                next_song_index = randrange(len(music_queue))

            url = music_queue[next_song_index][0]
            handler = self.get_url_handler(url)
            if handler is not None:
                (source, title) = handler[0]().get_song_info(url)
                self.set_now_playing(title)
                music_queue.pop(next_song_index)
                self.set_queue(music_queue)
                self.vc.play(discord.FFmpegPCMAudio(source, **self.FFMPEG_OPTIONS), after = lambda e: self.play_next())
        else:
            self.is_playing = False

    async def play_music(self, ctx):
        music_queue = self.get_queue()

        if len(music_queue) > 0:
            self.is_playing = True

            next_song_index = 0
            if self.shuffle:
                next_song_index = randrange(len(music_queue))

            url = music_queue[next_song_index][0]
            handler = self.get_url_handler(url)
            if handler is not None:
                (source, title) = handler[0]().get_song_info(url)

                if self.vc == "" or not self.vc.is_connected() or self.vc == None:
                    voice_channel = self.find_voice_channel(ctx, music_queue[next_song_index][2])
                    self.vc = await voice_channel.connect()
                else:
                    voice_channel = self.find_voice_channel(ctx, music_queue[next_song_index][2])
                    await self.vc.move_to(voice_channel)

                self.set_now_playing(title)
                await self.send_message(ctx, f"Now Playing: {title}")
                self.vc.play(discord.FFmpegPCMAudio(source, **self.FFMPEG_OPTIONS), after = lambda e: self.play_next())
                music_queue.pop(next_song_index)
                self.set_queue(music_queue)
        else:
            self.is_playing = False

    @commands.command(name = "play", help = "Searches or plays link")
    async def play(self, ctx, *args):
        query = " ".join(args)
        if ctx.author.voice is None:
            await self.send_message(ctx, MSG_VOICE_NOT_CONNECTED)
        else:
            voice_channel = ctx.author.voice.channel

            (handler, callback) = self.get_url_handler(query)
            await callback(handler(), query, ctx, voice_channel)

            # elif query.startswith("local"):
            #     file_name = query[6:]
            #     file_path = f"/music/{file_name}"
            #     if os.path.exists(file_path):
            #         title = os.path.splitext(file_name)[0]
            #         self.append_queue([file_path, title, voice_channel.id])
            #         await self.send_message(ctx, f"{file_path} Added to the Queue")
            #         if self.is_playing == False:
            #             await self.play_music(ctx)
            if handler is None:
                results = YoutubeHandler().search(query)

                # default first choice, unless there is more than one option
                if len(results) == 1:
                    url = results[0][0]
                    title = results[0][1]
                    self.append_queue([url, title, voice_channel.id])
                    await self.send_message(ctx, f"{title} Added to the Queue")
                
                else:
                    searchresults = ""
                    for i in range(0, len(results)):
                        searchresults += f"{i + 1}  -  {results[i][1]}\n"

                    message = await self.send_message(ctx, searchresults)

                    def check(reply):
                        return reply.content.startswith(self.bot.command_prefix) and reply.author == ctx.author and reply.channel == ctx.channel

                    rchoice = await self.bot.wait_for("message", check = check, timeout = 15)
                    choice = int(rchoice.content[len(self.bot.command_prefix):])

                    await message.delete() # delete search result message
                    url = results[choice-1][0]
                    await self.play(ctx, url) # pass selected url back to normal url handler

                if self.is_playing == False:
                    await self.play_music(ctx)
    
    @commands.command(name = "stream", help = "Streams audio from live stream URL")
    async def stream(self, ctx, url):
        if ctx.author.voice is None:
            await self.send_message(ctx, MSG_VOICE_NOT_CONNECTED)
        else:
            await self.send_message(ctx, f"{url} Added to the Queue")
            voice_channel = ctx.author.voice.channel
            self.append_queue([url, url, voice_channel.id])
            if self.is_playing == False:
                await self.play_music(ctx)
    
    @commands.command(name = "shuffle", help = "Shuffles queue until disabled")
    async def shuffle(self, ctx):
        if self.shuffle == False:
            self.shuffle = True
            await self.send_message(ctx, "Shuffle Enabled")
        else:
            self.shuffle = False
            await self.send_message(ctx, "Shuffle Disabled")

    @commands.command(name = "now", help = "Shows current song")
    async def now(self, ctx):
        if self.is_playing == False:
            await self.send_message(ctx, f"Not Playing")
        else:
            await self.send_message(ctx, f"Now Playing: {self.get_now_playing()}")
    
    @commands.command(name = "queue", help = "Shows queue")
    async def queue(self, ctx):
        music_queue = self.get_queue()
        retval = ""
        for i in range(0, len(music_queue)):
            retval += f"{i+1}  -  {music_queue[i][1]}\n"
            if len(retval) >= MAX_MESSAGE_LENGTH:
                break

        if retval != "":
            message = f"Now Playing: {self.get_now_playing()}\n{retval}"
            if len(message) >= MAX_MESSAGE_LENGTH - 4: # len("...")
                message = message[:MAX_MESSAGE_LENGTH - 4].rpartition('\n')[0] + '\n...'
            await self.send_message(ctx, message)
        else:
            if self.is_playing == True:
                await self.send_message(ctx, f"Now Playing: {self.get_now_playing()}\nQueue Empty")
            elif self.is_playing == False:
                await self.send_message(ctx, "Not Playing and Queue Empty")

    @commands.command(name = "skip", help = "Skips current song")
    async def skip(self, ctx):
        await self.send_message(ctx, "Current Song Skipped")
        self.vc.stop()
        await self.play_music(ctx)    

    @commands.command(name = "skipto", help = "Skips to selected index in queue")
    async def skipto(self, ctx, *args):
        num = "".join(*args)
        if self.vc != "" and self.vc:
            if int(num) == 1:
                await self.send_message(ctx, "Current Song Skipped")
                self.vc.stop()
                await self.play_music(ctx)
            elif int(num) >= 1:
                await self.send_message(ctx, f"Skipped {num} Songs")
                self.vc.stop()
                music_queue = self.get_queue()
                del music_queue[0:(int(num)-1)]
                self.set_queue(music_queue)
                await self.play_music(ctx)
            else:
                await self.send_message(ctx, "Current Song Skipped")
                self.vc.stop()
                await self.play_music(ctx)                
   
    @commands.command(name = "remove", help = "Removes song from queue at selected index")
    async def remove(self, ctx, *args):
        i = "".join(*args)
        music_queue = self.get_queue()
        await self.send_message(ctx, f"{music_queue[i][1]} Removed from the Queue")
        music_queue.pop(i)
        self.set_queue(music_queue)
    
    @commands.command(name = "pause", help = "Pauses music")
    async def pause(self, ctx):       
        self.vc.pause()
        await self.send_message(ctx, "Paused Music")
                 
    @commands.command(name = "resume", help = "Resumes music")
    async def resume(self, ctx):
        self.vc.resume()
        await self.send_message(ctx, "Resumed Music")
        
    @commands.command(name = "stop", help = "Stops music and clears queue")
    async def stop(self, ctx):
        if self.vc:
            self.vc.stop()
        self.set_queue([])
        self.shuffle = False
        await self.send_message(ctx, MSG_MUSIC_STOP)

    @commands.command(name = "leave", help = "Stops music, clears queue and leaves")
    async def leave(self, ctx):
        if self.vc and self.vc.is_connected():
            self.vc.stop()
            self.set_queue([])
            await self.send_message(ctx, "Leaving voice channel")
            await self.vc.disconnect(force=True)

    async def send_message(self, ctx, message):
        self.last_text_channel = ctx.channel
        return await ctx.send(message)

    def get_queue(self):
        byte_string = self.redisClient.get('music_queue')
        json_string = (byte_string or b"[]").decode('utf8')
        return json.loads(json_string)

    def set_queue(self, queue):
        self.redisClient.set('music_queue', json.dumps(queue))

    def append_queue(self, item):
        music_queue = self.get_queue()
        music_queue.append(item)
        self.set_queue(music_queue)

    def get_now_playing(self):
        return self.redisClient.get('now_playing').decode('utf8')

    def set_now_playing(self, now_playing):
        self.redisClient.set('now_playing', now_playing)

    def find_voice_channel(self, ctx, id):
        vc = next((x for x in ctx.guild.voice_channels if x.id == id), None)
        self.save_voice_channel(id)
        return vc

    def save_voice_channel(self, id):
        self.redisClient.set('voice_channel', id)

    def get_url_handler(self, query):
        for handler in self.handlers:
            pattern = handler[0]
            regex = re.compile(pattern)
            if regex.search(query) is not None:
                return handler[1] # return type and callback
        return None


def RunBot():
    COMMAND_PREFIX = os.getenv("COMMAND_PREFIX") or "!"

    bot = commands.Bot(command_prefix = COMMAND_PREFIX)
    cassette = Cassette(bot)
    bot.add_cog(cassette)

    @bot.command(name = "ping", help = "Shows bot latency")
    async def ping(ctx):
        await ctx.send(f"Latency: {round(bot.latency * 1000)}ms")

    @bot.event
    async def on_ready():
        print("Bot Online")
        cassette.redisClient = redis.Redis(
            host='172.32.1.4',
            port=6379,
            db=0,
            ssl=False,
            ssl_cert_reqs=None
        )
        cassette.redisClient.set("name", os.getenv("BOTNAME"))
        cassette.set_now_playing("")

        def event_handler(msg):
            if cassette.last_text_channel:
                cassette.last_text_channel.send(msg)

        cassette.redisSubscriber = cassette.redisClient.pubsub()
        subscribe_key = '*'
        cassette.redisSubscriber.psubscribe(**{subscribe_key: event_handler})

        cassette.redisSubscriber.run_in_thread(sleep_time=.01)

        await bot.change_presence(activity = discord.Activity(type = discord.ActivityType.listening, name = f"Music | {COMMAND_PREFIX}help"))
    
    bot.run(os.getenv("TOKEN"))

