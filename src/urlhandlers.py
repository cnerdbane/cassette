from abc import ABC, abstractmethod, abstractproperty
from yt_dlp import YoutubeDL

SEARCH_RESULT_COUNT = 10

class BaseUrlHandler(ABC):

	@abstractproperty
	def search_prefix(self):
		pass

	@abstractmethod
	def _get_best_audio_format(self, info):
		pass

	def get_song_info(self, url):
		ydl_opts = {'format': 'bestaudio' }
		with YoutubeDL(ydl_opts) as ydl:
			info = ydl.extract_info(url, download=False)
			return (self._get_best_audio_format(info)['url'], info['fulltitle'])

	def get_song_title(self, url):
		with YoutubeDL({}) as ydl:
			info = ydl.extract_info(url, download=False, process=False)
			return info['title']

	def get_playlist_urls(self, url):
		ydl_opts = {'format': 'bestaudio', 'playlistend': 20 }
		with YoutubeDL(ydl_opts) as ydl:
			results = ydl.extract_info(url, download=False, process=False, extra_info={'entries':'title'})
			return [(result['url'], result['title'] if 'title' in result else f"`{result['url']}`") for result in results['entries']]
	
	def search(self, query):
		with YoutubeDL({}) as ydl:
			results = ydl.extract_info(f"{self.search_prefix}{SEARCH_RESULT_COUNT}:{query}", download=False, process=False)
			return [(result['url'], result['title'] if 'title' in result else f"`{result['url']}`") for result in results['entries']]


class YoutubeHandler(BaseUrlHandler):

	@property
	def search_prefix(self):
		return "ytsearch"

	def _get_best_audio_format(self, info):
		return next(filter(lambda f: f['acodec'] != 'none' and f['vcodec'] == 'none', info['formats'][::-1]), None)


class SoundcloudHandler(BaseUrlHandler):

	@property
	def search_prefix(self):
		return "scsearch"

	def _get_best_audio_format(self, info):
		return info['formats'][-1]
