const redis = require('redis');
const express = require('express');
const app = express();
const redisClient = redis.createClient({
	url: 'redis://172.32.1.4:6379',
});

redisClient.on('connect', () => console.log('Redis Connected!'));
redisClient.on('error', (err) => console.log('Redis Client Error', err));

redisClient.connect();

app.get('/getName', async (req, res) => {
	const name = await redisClient.get('name');
	res.status(200).send(name);
});

app.get('/listQueue', async (req, res) => {
	const musicQueueJson = await redisClient.get('music_queue');
	const musicQueue = JSON.parse(musicQueueJson);
	res.status(200).send(musicQueue.map(songInfo => songInfo[1]));
});

app.get('/nowPlaying', async (req, res) => {
	const nowPlaying = await redisClient.get('now_playing');
	res.status(200).send(nowPlaying);
});

const server = app.listen(80, () => {
	const host = server.address().address
	const port = server.address().port
	console.log("Cassette backend listening at http://%s:%s", host, port)
});